Source: openrazer
Maintainer: Dylan Aïssi <daissi@debian.org>
Section: misc
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dkms,
               python3,
               python3-setuptools
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/openrazer
Vcs-Git: https://salsa.debian.org/debian/openrazer.git
Homepage: https://openrazer.github.io/
Rules-Requires-Root: no

Package: openrazer-meta
Architecture: all
Depends: ${misc:Depends},
         openrazer-driver-dkms (= ${binary:Version}),
         openrazer-daemon (= ${binary:Version}),
         python3-openrazer (= ${binary:Version})
Suggests: openrazer-doc (= ${binary:Version})
Description: OpenRazer peripheral drivers (metapackage)
 OpenRazer is a collection of GNU/Linux drivers for the Razer devices.
 Supported devices include keyboards, mice, mouse-mats, headsets and
 various other devices.
 .
 This package is a metapackage which depends on the OpenRazer driver and
 userspace daemon, plus a Python library and documentation.

Package: openrazer-driver-dkms
Architecture: all
Section: kernel
Depends: ${misc:Depends},
         dkms,
         udev
# To avoid problem with old upstream deb packages
Conflicts: openrazer-kernel-modules-dkms
Provides: openrazer-kernel-modules-dkms
Replaces: openrazer-kernel-modules-dkms
Description: OpenRazer peripheral drivers (DKMS)
 OpenRazer is a collection of GNU/Linux drivers for the Razer devices.
 Supported devices include keyboards, mice, mouse-mats, headsets and
 various other devices.
 .
 This package provides the source code for the OpenRazer kernel module to be
 build with dkms. Kernel sources or headers are required to compile this
 module.
 .
 Please read the Troubleshooting Guide in
 /usr/share/doc/openrazer-driver-dkms/README.Debian.

Package: openrazer-daemon
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         openrazer-driver-dkms (= ${binary:Version}),
         python3-dbus,
         python3-gi,
         python3-pyudev,
         python3-setproctitle,
         python3-notify2,
         python3-daemonize (>= 2.4.0),
         gir1.2-gtk-3.0,
         xautomation,
         dbus-user-session
Recommends: python3-openrazer (= ${binary:Version})
Description: OpenRazer peripheral drivers (daemon)
 OpenRazer is a collection of GNU/Linux drivers for the Razer devices.
 Supported devices include keyboards, mice, mouse-mats, headsets and
 various other devices.
 .
 This package provides a user-space daemon used to interface with the driver.
 It contains a systemd user unit and an AppStream file.

Package: python3-openrazer
Architecture: all
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         openrazer-daemon (= ${binary:Version}),
         python3-dbus,
         python3-gi,
         python3-numpy
Description: OpenRazer peripheral drivers (Python 3)
 OpenRazer is a collection of GNU/Linux drivers for the Razer devices.
 Supported devices include keyboards, mice, mouse-mats, headsets and
 various other devices.
 .
 This package contains a library for interacting with the OpenRazer daemon.

Package: openrazer-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: OpenRazer peripheral drivers (documentation)
 OpenRazer is a collection of GNU/Linux drivers for the Razer devices.
 Supported devices include keyboards, mice, mouse-mats, headsets and
 various other devices.
 .
 This package contains scripts on how to interact with the daemon and
 the driver.
